package ar.com.gestionit.db;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSourceFactory;

import ar.com.gestionit.encrypt.EncryptFactory;

public class AccessDB implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 4705993334307035866L;
	public static DataSource ds;

	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

	public AccessDB() {

		if (ds == null) {
			try {
				EncryptFactory desencriptador = new EncryptFactory("DES");

				Properties propiedadesPoolConexion = new Properties();
				Properties propiedadesClientCfg = null;

				propiedadesPoolConexion.load(this.getClass().getClassLoader().getResourceAsStream("database.properties"));

				// Si el archivo database.properties tiene la propiedad
				// "CLIENT.CFG",
				// entonces obtener el usuario y clave del archivo client.cfg y
				// desencriptarlos de la manera Genexus.
				// Sino obtener el usuario y clave del archivo
				// database.properties
				// y desencriptarlos de la manera tradicional.
				if (propiedadesPoolConexion.getProperty("CLIENT.CFG") != null) {
					propiedadesClientCfg = new Properties();
					propiedadesClientCfg.load(this.getClass().getClassLoader().getResourceAsStream(propiedadesPoolConexion.getProperty("CLIENT.CFG")));

					Logger.getLogger(AccessDB.class.getName()).log(Level.INFO, "user : " + propiedadesClientCfg.getProperty("USER_ID"));

					propiedadesPoolConexion.setProperty("username", desencriptador.desencriptarConMetodoGenexus(propiedadesClientCfg.getProperty("USER_ID"), "USER_ID"));
					propiedadesPoolConexion.setProperty("password", desencriptador.desencriptarConMetodoGenexus(propiedadesClientCfg.getProperty("USER_PASSWORD"), "USER_PASSWORD"));
				} else {
					Logger.getLogger(AccessDB.class.getName()).log(Level.INFO, "user : " + propiedadesPoolConexion.get("username"));
					try{
						propiedadesPoolConexion.setProperty("username", desencriptador.decrypt((String) propiedadesPoolConexion.get("username")));
						propiedadesPoolConexion.setProperty("password", desencriptador.decrypt((String) propiedadesPoolConexion.get("password")));
					}catch (IllegalArgumentException e) {
						
					}
					
				}

				// Crear el data source
				ds = BasicDataSourceFactory.createDataSource(propiedadesPoolConexion);

			} catch (IOException ex) {
				Logger.getLogger(AccessDB.class.getName()).log(Level.SEVERE, null, ex);
			} catch (Exception ex) {
				Logger.getLogger(AccessDB.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

	}
}

package ar.com.gestionit.servlet;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.commons.logging.Log;
import ar.com.gestionit.mail.*;
import ar.com.gestionit.templates.TemplatesHTML;
import ar.com.gestionit.util.Logs;

public class HelloServlet extends HttpServlet implements Serializable {

	private static final long serialVersionUID = 9102991650164972072L;
	private String cant_thread;
	private static Log LOGGER = Logs.obtenerLog(HelloServlet.class);

	@Override
	public void init() throws ServletException {

		super.init();

		/*
		 * Esta clase levanta todo lo que se va a usar para el env�o de los
		 * mails del disco (templates HTML, STMP, usuario, contrase�a, etc)
		 */
		
		TemplatesHTML.levantarTemplates();
		cargoThread();
	}

	private void cargoThread() {

		MailSender mails[] = new MailSender[3];
		
		ResourceBundle em = ResourceBundle.getBundle("envioMail");
		cant_thread = em.getString("cant_thread");
		
		try{
			LOGGER.info("***---*** Inicio de la aplicacion Envio de Mails ***---***");
			LOGGER.info("*** Tiempo de espera si no hay mails para enviar (en milisegundos): " + em.getString("wait_for_mail") + "***");
			LOGGER.info("*** Cantidad de Threads e utilizar: " + cant_thread + "***");
		}catch(Throwable e){
			LOGGER.error("", e);
		}
		

		for (int i = 0; i < Integer.parseInt(cant_thread); i++) {
			// La primera vez van a ser null...
			if (mails[i] == null) {				
				try {
					mails[i] = new MailSender(i);
					mails[i].start();
					
				} catch (Throwable e) {
					LOGGER.error("", e);
					}
			} else {
				// Si no son null, me fijo si ya terminaron y si es asi,
				// creo
				// uno nuevo...

				if (!mails[i].isAlive()) {					
					try {
						mails[i].run();
												
					} catch (Throwable e) {
						LOGGER.error("", e);
					}

				}
			}
		}
	}
}

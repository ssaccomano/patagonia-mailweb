package ar.com.gestionit.Exception;

public class ExcepcionGeneral extends Exception {
	
	private static final long serialVersionUID = -2813575946965248074L;

	public ExcepcionGeneral(String mensaje){
		super(mensaje);
	}
	
}

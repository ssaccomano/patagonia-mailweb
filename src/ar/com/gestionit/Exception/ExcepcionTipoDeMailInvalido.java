package ar.com.gestionit.Exception;

public class ExcepcionTipoDeMailInvalido extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7126082838250850783L;

	public ExcepcionTipoDeMailInvalido(String mensaje){
		super(mensaje);
	}
	
	

}

package ar.com.gestionit.Exception;

public class ExcepcionFirmaInexistente extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4609814895901164390L;

	public ExcepcionFirmaInexistente(String mensaje){
		super(mensaje);
	}
	
	

}

package ar.com.gestionit.Exception;

import java.io.Serializable;

// Esta excepcio es usada para enviar una notificacion por mail
public class ExcepcionErrorConexionAS400 extends Exception implements Serializable{

	private static final long serialVersionUID = 320761460679231399L;
	
	public ExcepcionErrorConexionAS400(String mensaje){
		super(mensaje);
	}

}

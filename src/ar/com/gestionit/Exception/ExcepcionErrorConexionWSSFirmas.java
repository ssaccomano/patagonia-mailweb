package ar.com.gestionit.Exception;

public class ExcepcionErrorConexionWSSFirmas extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 610110663388223052L;

	public ExcepcionErrorConexionWSSFirmas(String mensaje){
		super(mensaje);
	}
	
	

}

package ar.com.gestionit.util;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

public class Logs implements Serializable{



	/**
	 * 
	 */
	private static final long serialVersionUID = 5237087594434564982L;

	public static Log obtenerLog(Class cls) {
		Log log = LogFactory.getLog(cls);
		try {
			PropertyConfigurator.configure(System.getProperty("catalina.home").replaceAll("\\\\", "/") +
					Contexto.obtenerWebPropertiesLocal("WEBIB_PATH") + 
					Contexto.obtenerWebProperties("LOG4J_PROP_FILE"));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return log;
	}
}

package ar.com.gestionit.util;

/*
 * Herramientas Generales con acceso al contexto.
 */
import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;

/*import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ar.com.gestionit.wsscm.util.ClienteAxis2;*/

public class Contexto  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1955261536826061669L;

	/**
	 * 
	 */
	
	//private static Log log = LogFactory.getLog(ClienteAxis2.class);

	
	public Contexto() {}
    
  /*
   * Obtinene variables de inicio declaradas en el web.xml
   */
	/*public static String obtenerInitParam(String param) {
		FacesContext facesContext       = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		ServletContext servletContext   = (ServletContext) externalContext.getContext();
		return servletContext.getInitParameter(param).toString();
	}*/

  /*
   * recupera un parametro del request
   */
   /*public static String obtenerParametroHttp(String param) {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
		return req.getParameter(param)!= null ? req.getParameter(param) : null;
   }*/

   /*
    * agrega atributo a request
    */
    /*public static void agregarAtributoARequest(String clave, String valor) {
 		FacesContext fc = FacesContext.getCurrentInstance();
 		HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
 		req.setAttribute(clave, valor);
    }*/

    /*
     * recupera atributo desde el request
     */
    /*public static String obternerAtributoDeRequest(String clave) {
  		FacesContext fc = FacesContext.getCurrentInstance();
  		HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
 		return req.getAttribute(clave)!= null ? (String) req.getAttribute(clave) : "";
    } */  
  /*
   * Devuelve valor guardado como atributo de sesion
   */
   /*public static String obtenerAtributoSesion (String param) {
	   //FacesContext fc = FacesContext.getCurrentInstance();
	   //HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
	   //return (String) req.getSession(true).getAttribute(param);
	   return (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(param);
   }*/

   /*
    * Devuelve valor guardado como atributo de sesion
    */
    /*public static void agregarAtributoASesion (String key, String value) {
 	   FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, value);
    }*/

   
   /*
    * Devuelve valor de archivo de propiedades web (webcmLocal.properties)
    */
   public static String obtenerWebPropertiesLocal (String key) {
		ResourceBundle webcmConf = ResourceBundle.getBundle("envioMailLocal");
		return webcmConf.getString(key);
   }

   /*
    * Devuelve valor de archivo de propiedades web (webcm.properties)
    */
   public static String obtenerWebProperties (String key) {
		ResourceBundle webcmConf = ResourceBundle.getBundle("envioMail");
		return webcmConf.getString(key);
   }
   
   /*
    * Devuelve valor del archivo de propiedades e idioma pasado por parametro
    */
   public static String obtenerWebProperties (String propFile, String key, Locale local) {
		ResourceBundle webcmConf = ResourceBundle.getBundle(propFile,local);
		return webcmConf.getString(key);
   }
   
   /*public static void enviarMensajeErr(String key, Locale local )  {
		FacesMessage mensaje = new FacesMessage();
		ResourceBundle rb    = ResourceBundle.getBundle("ar.com.gestionit.webcm.recursos.mensajes",local);
		String         msg   = rb.getString(key);
		mensaje.setDetail(msg);
		mensaje.setSummary(msg);
		mensaje.setSeverity(FacesMessage.SEVERITY_ERROR);
		throw new ValidatorException(mensaje);
   }
   
   /*
    * Obtinene variables de inicio declaradas en el web.xml
    */
 	/*public static String obtenerRequestPath() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest req = (HttpServletRequest) fc.getExternalContext().getRequest();
        return req.getServletPath();
 	}
 	
    /*
     * Devuelve max duracion de intervalo inactivo de session
     */
    /*public static int obtenerMaxIntervaloInactivoWebSesion() {
    	HttpSession sess = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    	int intervalo = sess.getMaxInactiveInterval();
    	log.info("MaxInactiveInterval : " + intervalo);
    	return intervalo;
    }*/



}
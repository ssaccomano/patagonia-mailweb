/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.gestionit.quartz;

import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ar.com.gestionit.mail.alerta.ConexionAS400;
import ar.com.gestionit.mail.alerta.MailSenderAlert;
import ar.com.gestionit.servlet.HelloServlet;
import ar.com.gestionit.util.Logs;

/**
 *
 * @author JFelice
 */
public class TimerJob implements Job {
	
    private static Log LOGGER = Logs.obtenerLog(HelloServlet.class);
       
    public TimerJob() {
    }

    public void execute(JobExecutionContext context)
            throws JobExecutionException {
    	
    	//pregunto por issinconexion?
    	ConexionAS400 conEx = new ConexionAS400();    	
    	if (conEx.isSinConexion()){    		
    			//Invocacion de mail de alerta    		
    			MailSenderAlert mailAlerta = null;
    			try {
    				mailAlerta = new MailSenderAlert();
    			} catch (NumberFormatException e1) {    				
    				LOGGER.error("", e1);
    			} catch (SQLException e2) {
    				LOGGER.error("", e2);
    			}
    			mailAlerta.run();		
    		}                    
        }
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.gestionit.quartz;

import java.io.IOException;
import java.util.Date;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.logging.Log;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;

import ar.com.gestionit.mail.alerta.ConexionAS400;
import ar.com.gestionit.servlet.HelloServlet;
import ar.com.gestionit.util.Logs;

/**
 *
 * @author JFelice
 */
public class SchedulerMAilAlert extends GenericServlet {
	public static Scheduler sched = null;
	public static boolean inicializado = false;
	/**
	 * 
	 */
	private static final long serialVersionUID = 569954350963217211L;
	private static Log LOGGER = Logs.obtenerLog(HelloServlet.class);

    public void init(ServletConfig config) throws ServletException {
        super.init(config);       
        
      try {
		SchedulerMAilAlert.sched = StdSchedulerFactory.getDefaultScheduler();
	} catch (SchedulerException e2) {				
		 LOGGER.error("", e2);
	}
    try {                
        LOGGER.info("**--** Inicio de scheduled para Envio de Mails de Alerta **--**");            
    	} catch (Exception e) {            
    		LOGGER.error("", e);
    	}
    }
    
    public void service(ServletRequest arg0, ServletResponse arg1)
            throws ServletException, IOException {
    }

    public String getServletInfo() {
        return null;
    }

public static synchronized void inicializarSched() {
    JobDetail job = new JobDetail("job", "grupo", TimerJob.class);

    long ts = TriggerUtils.getNextGivenSecondDate(null, 15).getTime();

    SimpleTrigger trigger = new SimpleTrigger("trigger", "grupo", "job", "grupo",
    
            new Date(ts), null, SimpleTrigger.REPEAT_INDEFINITELY, 60000L * 30L);

    
	try {		
		sched.scheduleJob(job, trigger);		
		if (!ConexionAS400.sinConexion && !SchedulerMAilAlert.inicializado)
			sched.shutdown(true);
	} catch (SchedulerException e1) {
		 LOGGER.error("", e1);
	}

}
}

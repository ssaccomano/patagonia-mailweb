package ar.com.gestionit.encrypt;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.logging.Log;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import ar.com.gestionit.util.Logs;

import com.genexus.GXutil;
import com.genexus.util.Encryption;

public class EncryptFactory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -469733715806505848L;

	public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";

	public static final String DES_ENCRYPTION_SCHEME = "DES";

	public static final String DEFAULT_ENCRYPTION_KEY = "La cucaracha, la cucaracha, ya no puede caminar...";

	private static final String UNICODE_FORMAT = "UTF8";

	private KeySpec keySpec;

	private SecretKeyFactory keyFactory;

	private Cipher cipher;

	private String key = "7E2E22D26FF2989E2444852A85E57867";
	
	private static Log LOGGER = Logs.obtenerLog(EncryptFactory.class);

	public static void main(String[] args) {

		if (args.length < 1) {			
			return;
		}
		String passwd = args[0];

		try {
			EncryptFactory ef = new EncryptFactory("DES");
			String ver = ef.encrypt(passwd);
			String ver2 = ef.decrypt(ver);

		} catch (EncryptionException ee) {
			LOGGER.error("", ee);
		} catch (Exception e) {
			LOGGER.error("", e);
			;
		}
	}

	public EncryptFactory(String encryptionScheme) throws EncryptionException {
		this(encryptionScheme, DEFAULT_ENCRYPTION_KEY);
	}

	public EncryptFactory(String encryptionScheme, String encryptionKey)
			throws EncryptionException {

		if (encryptionKey == null)
			throw new IllegalArgumentException("Clave de Encripci�n nula");
		if (encryptionKey.trim().length() < 24)
			throw new IllegalArgumentException(
					"La Clave de encripci�n tiene menos de 24 caracteres");

		try {
			byte[] keyAsBytes = encryptionKey.getBytes(UNICODE_FORMAT);

			if (encryptionScheme.equals(DESEDE_ENCRYPTION_SCHEME)) {
				keySpec = new DESedeKeySpec(keyAsBytes);
			} else if (encryptionScheme.equals(DES_ENCRYPTION_SCHEME)) {
				keySpec = new DESKeySpec(keyAsBytes);
			} else {
				throw new IllegalArgumentException(
						"Esquema de encriptaci�n no soportado: "
								+ encryptionScheme);
			}

			keyFactory = SecretKeyFactory.getInstance(encryptionScheme);
			cipher = Cipher.getInstance(encryptionScheme);

		} catch (InvalidKeyException e) {
			throw new EncryptionException(e);
		} catch (UnsupportedEncodingException e) {
			throw new EncryptionException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException(e);
		} catch (NoSuchPaddingException e) {
			throw new EncryptionException(e);
		}

	}

	public String encrypt(String unencryptedString) throws EncryptionException {
		if (unencryptedString == null || unencryptedString.trim().length() == 0)
			throw new IllegalArgumentException(
					"El string a encriptar est� vacio o es nulo");

		try {
			SecretKey key = keyFactory.generateSecret(keySpec);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] cleartext = unencryptedString.getBytes(UNICODE_FORMAT);
			byte[] ciphertext = cipher.doFinal(cleartext);

			BASE64Encoder base64encoder = new BASE64Encoder();
			return base64encoder.encode(ciphertext);
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	public String decrypt(String encryptedString) throws EncryptionException, IllegalArgumentException {
		if (encryptedString == null || encryptedString.trim().length() <= 0)
			throw new IllegalArgumentException(
					"El string encriptado est� vacio o es nulo");

		try {
			SecretKey key = keyFactory.generateSecret(keySpec);
			cipher.init(Cipher.DECRYPT_MODE, key);
			BASE64Decoder base64decoder = new BASE64Decoder();
			byte[] cleartext = base64decoder.decodeBuffer(encryptedString);
			byte[] ciphertext = cipher.doFinal(cleartext);

			return bytes2String(ciphertext);
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	public String desencriptarConMetodoGenexus(String encryptedS, String prop) {
		String decryptedS = "";

		if (prop.equals("USER_PASSWORD")) {
			key = key.substring(16, 32) + key.substring(0, 16);
		}

		decryptedS = Encryption.decrypt64(encryptedS, key);
		decryptedS = GXutil.left(decryptedS, decryptedS.length()
				- Encryption.getCheckSumLength());

		return decryptedS;
	}

	private static String bytes2String(byte[] bytes) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			stringBuffer.append((char) bytes[i]);
		}
		return stringBuffer.toString();
	}

	public static class EncryptionException extends Exception {
		private static final long serialVersionUID = 8806719261054676439L;

		public EncryptionException(Throwable t) {
			super(t);
		}
	}

}

//package ar.com.gestionit.testunits;
//
//import java.io.IOException;
//import java.io.StringReader;
//
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.w3c.dom.Document;
//import org.xml.sax.InputSource;
//import org.xml.sax.SAXException;
//
//import ar.com.gestionit.mail.ProcesadorHTML;
//import junit.framework.TestCase;
//
//public class ProcesadorHTMLTest extends TestCase {
//
//	public void testDynamicTable() {
//		try {
//			//Generate the dynamic table content
//			StringBuffer input = new StringBuffer();
//			input.append("<TABLE>");
//			for(int i = 0; i < 5; i++)
//				input.append("<ROOT>")
//				.append("<IDBT>109997841</IDBT>")
//				.append("<NCLIENTE>ARMADA ARGENTINA</NCLIENTE>")
//				.append("<PRODUCTO>Producto1</PRODUCTO>")
//				.append("<NCONV>0000136</NCONV>")
//				.append("</ROOT>");
//			input.append("</TABLE>");
//			Document xmldoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(input.toString())));
//			
//			System.out.println(input.toString());
//			
//			//Build table
//			ProcesadorHTML processor = new ProcesadorHTML();
//			processor.init();
//			String table = processor.getDynamicTable(xmldoc, null);
//			
//			System.out.println(table);
//			System.out.println("The test case finishes successfully");
//		} catch (SAXException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (ParserConfigurationException e) {
//			e.printStackTrace();
//		}
//	}
//}

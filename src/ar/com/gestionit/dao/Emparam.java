package ar.com.gestionit.dao;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ar.com.gestionit.Exception.ExcepcionErrorEnParametros;
import ar.com.gestionit.Exception.ExcepcionErrorEnXML;
import ar.com.gestionit.db.AccessDB;
import ar.com.gestionit.util.Logs;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class Emparam implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 763022127665998500L;
	private ResultSet rs = null;
	private AccessDB db = null;
	private Connection cnx = null;
	private Statement statement = null;
	private static Log LOGGER = Logs.obtenerLog(Emparam.class);
	
	
	public Emparam (String epnum) throws SQLException {
				
		db = new AccessDB();				
		cnx = db.getConnection();		
		statement = cnx.createStatement();				
		rs = statement.executeQuery("select epblk1 from emparam where epsisor='SAT' and epnum = " + epnum + " order by epnumbk");
		
	}		
	
	//***** Gets de todos los Campos de la Tabla Emparam *****/
		
		public Document getEmparamXml() throws ExcepcionErrorEnParametros, ExcepcionErrorEnXML {
			String bloque = "";
			Document xmldoc = null;
			
			try{
				if (rs != null) {
					while (rs.next()){
						bloque += rs.getString("EPBLK1").trim();					
					}
				
					//XML con los Datos a copiar al mail
					//xmldoc				
					xmldoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(bloque)));
				}else{
					cerrar();
					
					throw new ExcepcionErrorEnParametros("Error: ResultSet en estado incorrecto.");				
				}
				/***************************************************************************************/
//				xmldoc = new DocumentImpl();
//
//		        Element root = xmldoc.createElement("root");
//
//		        Element e = xmldoc.createElement("CADJ");
//		        e.setTextContent("S3");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("CENT");
//		        e.setTextContent("R3");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("RFCL");
//		        e.setTextContent("REF52007001");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("DC1");
//		        e.setTextContent("DNI 23.232.323");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("NOM1");
//		        e.setTextContent("GUALBERTO  AUT 2 DE SUDOESTE S.A.");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("DC2");
//		        e.setTextContent("DNI 24.242.424");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("NOM1");
//		        e.setTextContent("JUAN PEREZ AUT 1 DE SUDOESTE S.A.");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("DC3");
//		        e.setTextContent("");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("NOM3");
//		        e.setTextContent("");
//		        root.appendChild(e);
//		        		        
//		        e = xmldoc.createElement("MS");
//		        e.setTextContent("$");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("B2B");
//		        e.setTextContent("N");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("FCH3");
//		        e.setTextContent("6 de Mayo de 2008");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("FCH4");
//		        e.setTextContent("2008/05/06");
//		        root.appendChild(e);
//		        		        		        
//		        e = xmldoc.createElement("FEEJ");
//		        e.setTextContent("5 de Mayo de 2008");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("CCE");
//		        e.setTextContent("N");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("RFOP");
//		        e.setTextContent("S");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("ROP");
//		        e.setTextContent("Facturas: 0001A00000121");
//		        root.appendChild(e);
//		        		        		        
//		        e = xmldoc.createElement("CLI");
//		        e.setTextContent("Cuenta:100074997-  1");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("IMPN");
//		        e.setTextContent("471.697,00");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("CTA");
//		        e.setTextContent("");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("ACC");
//		        e.setTextContent("a transferira");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("NOMB");
//		        e.setTextContent("BANCO PATAGONIA S.A.");
//		        root.appendChild(e);
//		        	        
//		        e = xmldoc.createElement("EMOK");
//		        e.setTextContent("N");
//		        root.appendChild(e);
//		        
//		        e = xmldoc.createElement("MSUC");
//		        e.setTextContent("S");
//		        root.appendChild(e);
		       	       		       				
				/***************************************************************************************/
				//System.out.println(root.toString());
				
				// obtener string de dom

//	            TransformerFactory tFactory = TransformerFactory.newInstance();
//
//	            Transformer transformer = tFactory.newTransformer();
//
//	            DOMSource source = new DOMSource(root);
//
//	            StringWriter sw = new StringWriter();
//
//	            StreamResult result = new StreamResult(sw);
//
//	            transformer.transform(source, result);
//
//	            String xmlString = sw.toString();
//	            	            
//	            xmldoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xmlString)));
//								
		       	//xmldoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(root.toString())));
				//imprimirXML(xmldoc);
				
			} catch (SQLException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnParametros(e.getMessage());
			} catch (IOException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnXML(e.getMessage());
			} catch (SAXException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnXML(e.getMessage());
			} catch (ParserConfigurationException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnXML(e.getMessage());
			}
			
			cerrar();			
			return xmldoc;
			
			
		}
		
		private void imprimirXML (Document xmldoc) throws IOException{
			//Salida del XML
			OutputFormat format = new OutputFormat(xmldoc);
	        format.setLineWidth(65);
	        format.setIndenting(true);
	        format.setIndent(2);
	        XMLSerializer serializer = new XMLSerializer(System.out, format);
	        serializer.serialize(xmldoc);
		}
			
		public void cerrar () {
			if (rs != null){
				try {					
					rs.close();					
				}catch (Throwable e) {					
					LOGGER.error("", e);
				}			
			}
			if (statement != null) {
				try {					
					statement.close();
				} catch (Throwable e) {
					LOGGER.error("", e);
				}
			}
			if (cnx != null) {
				try {
					cnx.close();					
				} catch (Throwable e) {
					LOGGER.error("", e);
				}
			}
			rs = null;
			statement = null;
			cnx = null;		
		}
	
}

package ar.com.gestionit.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.logging.Log;

import ar.com.gestionit.db.AccessDB;
import ar.com.gestionit.util.Logs;

/***** Datos Generales del Mail *****/
public class Emenvml implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3439826601790029698L;
	private ResultSet rs = null;
	private AccessDB db = null;
	private Connection cnx = null;
	private Statement statement = null;
	
	private String emsisor,emtipml, emdesml, emestad, emcntri, emfchcr, emhorcr, emusrcr, emde = null, emdetd = null;
	private static Log LOGGER = Logs.obtenerLog(Emenvml.class);
	
	
	public Emenvml (String Emnum) throws SQLException{
		
		db = new AccessDB();
		cnx = db.getConnection();
		statement = cnx.createStatement();
		rs = statement.executeQuery("select * from emenvml where emsisor='SAT' and emestad = 2 and emnum = " + Emnum);
		
		if(!rs.next()){
			throw new SQLException("Error: No se obtuvieron resultados para la consulta: select * from emenvml where emsisor='SAT' and emestad = 2 and emnum = " + Emnum);
		}		
	}
	
	public void cerrar () {
		if (rs != null){
			try {				
				rs.close();
			}catch (Throwable e) {				
				LOGGER.error("", e);
			}			
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (Throwable e) {
				LOGGER.error("", e);
			}
		}
		if (cnx != null) {
			try {
				cnx.close();				
			} catch (Throwable e) {
				LOGGER.error("", e);
			}
		}	
		rs = null;
		statement = null;
		cnx = null;
	}
	
//***** Gets de todos los Campos de la Tabla EMENVML *****/
	
	public String getEmsisor() throws SQLException {
		
		if(this.emsisor==null){
			if (rs != null) {
				emsisor = rs.getString("EMSISOR");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emsisor;
	}
	
	public int getEmtipml() throws SQLException {
					
		if(this.emtipml==null){
			if (rs != null) {
				emsisor = rs.getString("EMTIPML");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return Integer.parseInt(emsisor);
	}
	
	public String getEmestad() throws SQLException {
						
		if(this.emestad==null){
			if (rs != null) {
				emsisor = rs.getString("EMESTAD");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emestad;
	}
	
	public String getEmcntri() throws SQLException {
		
		if(this.emcntri==null){
			if (rs != null) {
				emcntri = rs.getString("EMCNTRI");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emcntri;
	}
	
	public String getEmfchcr() throws SQLException {
		
		if(this.emfchcr==null){
			if (rs != null) {
				emfchcr = rs.getString("EMFCHCR");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emfchcr;
	}

	public String getEmhorcr() throws SQLException {
		
		if(this.emhorcr==null){
			if (rs != null) {
				emhorcr = rs.getString("EMHORCR");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emhorcr;	
	}

	public String getEmusrcr() throws SQLException {
		
		if(this.emusrcr==null){
			if (rs != null) {
				emusrcr = rs.getString("EMUSRCR");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emusrcr;				
	}

	public String getEmde() throws SQLException {
		
		if(this.emde==null){
			if (rs != null) {
				emde = rs.getString("EMDE");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emde;	
	}

	public String getEmpara1() throws SQLException {
		if (rs != null) {
			return rs.getString("EMPARA1");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmpara2() throws SQLException {
		if (rs != null) {
			return rs.getString("EMPARA2");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmpara3() throws SQLException {
		if (rs != null) {
			return rs.getString("EMPARA3");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmcc1() throws SQLException {
		if (rs != null) {
			return rs.getString("EMCC1");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmcc2() throws SQLException {
		if (rs != null) {
			return rs.getString("EMCC2");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmcc3() throws SQLException {
		if (rs != null) {
			return rs.getString("EMCC3");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmcco1() throws SQLException {
		if (rs != null) {
			return rs.getString("EMCCO1");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmcco2() throws SQLException {
		if (rs != null) {
			return rs.getString("EMCCO2");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmcco3() throws SQLException {
		if (rs != null) {
			return rs.getString("EMCCO3");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmasunt() throws SQLException {
		if (rs != null) {
			return rs.getString("EMASUNT");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmcantb() throws SQLException {
		if (rs != null) {
			return rs.getString("EMCANTB");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmexadj() throws SQLException {
		if (rs != null) {
			return rs.getString("EMEXADJ");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmfchen() throws SQLException {
		if (rs != null) {
			return rs.getString("EMFCHEN");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}

	public String getEmhoren() throws SQLException {
		if (rs != null) {
			return rs.getString("EMHOREN");
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
	}
	
	public int getCantidadPara(){
		
		try{
			if(this.getEmpara1().trim().equalsIgnoreCase("")){
				return 0;
			}else if (this.getEmpara2().trim().equalsIgnoreCase("")){
				return 1;
			}else if (this.getEmpara3().trim().equalsIgnoreCase("")){
				return 2;
			}else{
				return 3;
			}
		}catch(Exception e){
			
			LOGGER.error("", e);
			return 0;
		}
		
	}
	
	public int getCantidadCC(){
		
		try{
			if(this.getEmcc1().trim().equalsIgnoreCase("")){
				return 0;
			}else if (this.getEmcc2().trim().equalsIgnoreCase("")){
				return 1;
			}else if (this.getEmcc3().trim().equalsIgnoreCase("")){
				return 2;
			}else{
				return 3;
			}
		}catch(Exception e){
			
			LOGGER.error("", e);
			return 0;
		}
		
	}
	
	public int getCantidadCCO(){
		
		try{
			if(this.getEmcco1().trim().equalsIgnoreCase("")){
				return 0;
			}else if (this.getEmcco2().trim().equalsIgnoreCase("")){
				return 1;
			}else if (this.getEmcco3().trim().equalsIgnoreCase("")){
				return 2;
			}else{
				return 3;
			}
		}catch(Exception e){
			
			LOGGER.error("", e);
			return 0;
		}
		
	}
	
	public String getNumOrdenPago() throws SQLException {
		try{
			if (rs != null) {
				return rs.getString("EMQOP");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}catch (Exception e) {
			LOGGER.error("", e);
		}
		return null;
	}
	
	public String getNumSubOrdenPago() throws SQLException {
		try{
			if (rs != null) {
				return rs.getString("EMQSO");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}catch (Exception e) {
			LOGGER.error("", e);
		}
		return null;
	}
	
	/**
	 * @return EMENVML.EMDETD content
	 * @throws SQLException
	 */
	public String getEmdetd() throws SQLException {
		
		if(this.emdetd==null){
			if (rs != null) {
				emdetd = rs.getString("EMDETD");
			}else{
				throw new SQLException("Error: ResultSet en estado incorrecto.");
			}
		}
		return emdetd;	
	}
}
package ar.com.gestionit.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.commons.logging.Log;

import ar.com.gestionit.db.AccessDB;
import ar.com.gestionit.util.Logs;

/***** Datos Generales de cada Comprobante *****/
public class Emcabcp implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 4228446636811202384L;
	/**
	 * 
	 */
	
	private ResultSet rs = null;
	private AccessDB db = null;
	private Connection cnx = null;
	private Statement statement = null;
	private HashMap<String, String> hsEmcabcp = null;
	private String ehnum = null;
	private static Log LOGGER = Logs.obtenerLog(Emcabcp.class);
		
	
	public Emcabcp (String Ehnum) throws SQLException{
		
		db = new AccessDB();
		cnx = db.getConnection();
		statement = cnx.createStatement();
		rs = statement.executeQuery("select * from emcabcp where ehsisor='SAT' and ehsol='EML' and ehnum = " + Ehnum);
		this.ehnum = Ehnum;
		hsEmcabcp = new HashMap<String, String>();
	}
	
	public HashMap<String, String> hs() throws SQLException {
		// TODO Auto-generated method stub
		
		if(!rs.next()){
			throw new SQLException("Error: No se obtuvieron resultados para la consulta: select * from emcabcp where ehsisor='SAT' and ehsol='EML' and ehnum = " + ehnum);
		}else{
			do {
				hsEmcabcp.put(rs.getString("EHNCOMP"), rs.getString("EHDESCP").trim());
			} while (rs.next());
		}
		
		cerrar();		
		return hsEmcabcp;
	}
	
	public void cerrar () {
		if (rs != null){
			try {
				rs.close();
			}catch (Exception e) {
				//agregar logeo
				LOGGER.error("", e);
			}			
		}
		if (statement != null) {
			try {
				statement.close();				
			} catch (Exception e) { 
				LOGGER.error("", e);
			}
		}
		if (cnx != null) {
			try {
				cnx.close();
			} catch (Exception e) {
				LOGGER.error("", e);
			}
		}
		rs = null;
		statement = null;
		cnx = null;		
	}

}

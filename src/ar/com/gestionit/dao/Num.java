package ar.com.gestionit.dao;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.logging.Log;

import ar.com.gestionit.Exception.ExcepcionErrorConexionAS400;
import ar.com.gestionit.db.AccessDB;
import ar.com.gestionit.mail.MailSender;
import ar.com.gestionit.mail.alerta.ConexionAS400;
import ar.com.gestionit.util.Logs;

public class Num implements Serializable{
		
	private static final long serialVersionUID = -2528100466080682211L;
	private AccessDB db1 = new AccessDB();
	private String num = null;
	private static Log LOGGER = Logs.obtenerLog(MailSender.class);
	ConexionAS400 conAS = new ConexionAS400();
	
	private void efectuarCall() throws SQLException{
						
		// *** Call al AS400 ***
	    CallableStatement sqlCall;	        	    
	    Connection con = db1.getConnection();
	    
	    //Si tiene Conexion, setea en falso para que no env�e mails de alerta
    	conAS.setSinConexion(false);
    	
	    sqlCall = con.prepareCall("CALL EM0002(?,?)");
	    	    	    	    
	    //parametros inout
	    //sqlCall.setInt(1, 0);
	    sqlCall.registerOutParameter(1, java.sql.Types.INTEGER);
	    sqlCall.registerOutParameter(2, java.sql.Types.INTEGER);
	    
	    sqlCall.setInt(1, 0);
	    sqlCall.setInt(2, 0);
	  
	    // ejecuto el stored procedure
	    sqlCall.execute();
    		    
	    // obtengo el NUM
	    this.num = Integer.toString(sqlCall.getInt(1));
	    
	    sqlCall.close();
	    if(con!=null && !con.isClosed()){
	    	con.close();
	    }
	}
		
	public String getNum() throws ExcepcionErrorConexionAS400 {		
				
		if(this.num==null || this.num.trim().equalsIgnoreCase("0")){
			try {
				efectuarCall();
			}catch (Throwable e) {
				//Seteo problemas de conexion
				conAS.setSinConexion(true);
				LOGGER.error("", e);				
				ExcepcionErrorConexionAS400 ex = new ExcepcionErrorConexionAS400(e.getMessage());
				ex.setStackTrace(e.getStackTrace());
				
				throw ex;
			}
		}else {
			return num;
		}	
		return num.trim();
	}   
	
}
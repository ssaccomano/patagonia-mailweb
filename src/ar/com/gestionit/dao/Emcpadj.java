package ar.com.gestionit.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.commons.logging.Log;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import ar.com.gestionit.db.AccessDB;
import ar.com.gestionit.util.Logs;

/***** Tabla de Comprobantes Adjuntos *****/
public class Emcpadj implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4603447374553241066L;
	private ResultSet rs = null;
	private AccessDB db = null;
	private Connection cnx = null;
	private Statement statement = null;
	private static Log LOGGER = Logs.obtenerLog(Emcpadj.class);
	
	
	public Emcpadj (String Ecnum) throws SQLException{
		
		db = new AccessDB();
		cnx = db.getConnection();
		statement = cnx.createStatement();
		rs = statement.executeQuery("select * from emcpadj where ecsisor='SAT' and ecsol='EML' ecnum = " + Ecnum);
			
	}
	
	public ArrayList<ByteArrayOutputStream> obtenerComprobantes() throws SQLException, DocumentException, IOException {
		
		String nroComp = null;
		String nroHoja = null;

		
		ArrayList<ByteArrayOutputStream> listaPdfsGenerados = new ArrayList<ByteArrayOutputStream>();
		ByteArrayOutputStream out = new ByteArrayOutputStream();	
		
		if (rs != null) { 
			//Genero un Array con las lineas del Comprobante
			rs.next();
			
			nroComp = rs.getString("ECNCOMP").trim();
			nroHoja = rs.getString("ECNHOJA").trim();
			
			Document document = new Document();
			PdfWriter.getInstance(document, out);
			
			document.open();
			
			document.add(new Paragraph(rs.getString("ECLINEA")));		
			
			while(rs.next()){
				
				while(!rs.isAfterLast() && rs.getString("ECNCOMP").trim().equalsIgnoreCase(nroComp) && rs.getString("ECNHOJA").trim().equalsIgnoreCase(nroHoja)){
					
					document.add(new Paragraph(rs.getString("ECLINEA")));					
					rs.next();
					
				}
				
				if(!rs.isAfterLast()){
					if(!rs.getString("ECNCOMP").trim().equalsIgnoreCase(nroComp)){
						
						// Cierro el documento y lo agrego al ArrayList que devolvere
						document.close();
						out.close();
						listaPdfsGenerados.add(out);
						
						// Creo un nuevo documento para escribir lo que sigue en el
						out = new ByteArrayOutputStream();
						document = new Document();
						PdfWriter.getInstance(document, out);
						document.open();
						
						// Actualizo los aux
						nroComp = rs.getString("ECNCOMP");
						nroHoja = rs.getString("ECNHOJA");
					}else{
						// Agrego una nueva hoja al comprobante
						document.newPage();
						nroHoja = rs.getString("ECNHOJA");
					}
				}
				
				document.add(new Paragraph(rs.getString("ECLINEA")));
				
			}
			
			document.close();
			out.close();
			listaPdfsGenerados.add(out);
			
		}else{
			throw new SQLException("Error: ResultSet en estado incorrecto.");
		}
		
		cerrar();		
		return listaPdfsGenerados;
		
	}
	
	public void cerrar () {
		if (rs != null){
			try {
				rs.close();
			}catch (Exception e) {
				//agregar logeo
				LOGGER.error("", e);
			}			
		}
		if (statement != null) {
			try {
				statement.close();				
			} catch (Exception e) {
				LOGGER.error("", e);
			}
		}
		if (cnx != null) {
			try {
				cnx.close();
			} catch (Exception e) {
				LOGGER.error("", e);
			}
		}	
		rs = null;
		statement = null;
		cnx = null;
	}
}

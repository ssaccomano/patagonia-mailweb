package ar.com.gestionit.dao;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ar.com.gestionit.Exception.ExcepcionErrorEnParametros;
import ar.com.gestionit.Exception.ExcepcionErrorEnXML;
import ar.com.gestionit.db.AccessDB;
import ar.com.gestionit.util.Logs;

public class Emdetavi implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 763022127665998500L;
	private ResultSet rs = null;
	private AccessDB db = null;
	private Connection cnx = null;
	private Statement statement = null;
	private static Log LOGGER = Logs.obtenerLog(Emdetavi.class);
	
	
	public Emdetavi (String emdidm) throws SQLException {
				
		db = new AccessDB();				
		cnx = db.getConnection();		
		statement = cnx.createStatement();				
		rs = statement.executeQuery("select EMDBLK from EMDETAVI where EMDSOR='SAT' and EMDIDM = " + emdidm + " order by EMDNLI");
		
	}		
	
	//***** Gets de todos los Campos de la Tabla Emparam *****/
		
		public Document getEmdetaviXml() throws ExcepcionErrorEnParametros, ExcepcionErrorEnXML {
			String bloque = "";
			Document xmldoc = null;
			
			try{
				if (rs != null) {
					bloque += "<TABLE>";
					while (rs.next()){
						bloque += rs.getString("EMDBLK").trim();					
					}
					bloque += "</TABLE>";
				
					//XML con los Datos a copiar al mail
					//xmldoc				
					xmldoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(bloque)));
				}else{
					cerrar();
					
					throw new ExcepcionErrorEnParametros("Error: ResultSet en estado incorrecto.");				
				}
				/***************************************************************************************/
			} catch (SQLException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnParametros(e.getMessage());
			} catch (IOException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnXML(e.getMessage());
			} catch (SAXException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnXML(e.getMessage());
			} catch (ParserConfigurationException e) {
				LOGGER.error("", e);
				throw new ExcepcionErrorEnXML(e.getMessage());
			}
			
			cerrar();			
			return xmldoc;
			
			
		}
		
		public void cerrar () {
			if (rs != null){
				try {					
					rs.close();					
				}catch (Throwable e) {					
					LOGGER.error("", e);
				}			
			}
			if (statement != null) {
				try {					
					statement.close();
				} catch (Throwable e) {
					LOGGER.error("", e);
				}
			}
			if (cnx != null) {
				try {
					cnx.close();					
				} catch (Throwable e) {
					LOGGER.error("", e);
				}
			}
			rs = null;
			statement = null;
			cnx = null;		
		}
	
}

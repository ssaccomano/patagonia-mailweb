package ar.com.gestionit.ClienteWS;

public class ServicioComprobanteProxy implements ar.com.gestionit.ClienteWS.ServicioComprobante {
  private String _endpoint = null;
  private ar.com.gestionit.ClienteWS.ServicioComprobante servicioComprobante = null;
  
  public ServicioComprobanteProxy() {
    _initServicioComprobanteProxy();
  }
  
  public ServicioComprobanteProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicioComprobanteProxy();
  }
  
  private void _initServicioComprobanteProxy() {
    try {
      servicioComprobante = (new ar.com.gestionit.ClienteWS.ServicioComprobanteServiceLocator()).getServicioComprobantePort();
      if (servicioComprobante != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicioComprobante)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicioComprobante)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicioComprobante != null)
      ((javax.xml.rpc.Stub)servicioComprobante)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ar.com.gestionit.ClienteWS.ServicioComprobante getServicioComprobante() {
    if (servicioComprobante == null)
      _initServicioComprobanteProxy();
    return servicioComprobante;
  }
  
  public ar.com.gestionit.ClienteWS.Comprobante[] getComprobante(long numeroDeOrdenDePago, long subnumeroDeOrdenDePago, int numeroMail) throws java.rmi.RemoteException{
    if (servicioComprobante == null)
      _initServicioComprobanteProxy();
    return servicioComprobante.getComprobante(numeroDeOrdenDePago, subnumeroDeOrdenDePago, numeroMail);
  }
  
  
}
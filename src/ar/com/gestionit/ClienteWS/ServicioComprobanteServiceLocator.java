/**
 * ServicioComprobanteServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ar.com.gestionit.ClienteWS;

public class ServicioComprobanteServiceLocator extends org.apache.axis.client.Service implements ar.com.gestionit.ClienteWS.ServicioComprobanteService {

    public ServicioComprobanteServiceLocator() {
    }


    public ServicioComprobanteServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ServicioComprobanteServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ServicioComprobantePort
    private java.lang.String ServicioComprobantePort_address = "http://localhost:8080/WSCPADJ/ServicioComprobante";

    public java.lang.String getServicioComprobantePortAddress() {
        return ServicioComprobantePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ServicioComprobantePortWSDDServiceName = "ServicioComprobantePort";

    public java.lang.String getServicioComprobantePortWSDDServiceName() {
        return ServicioComprobantePortWSDDServiceName;
    }

    public void setServicioComprobantePortWSDDServiceName(java.lang.String name) {
        ServicioComprobantePortWSDDServiceName = name;
    }

    public ar.com.gestionit.ClienteWS.ServicioComprobante getServicioComprobantePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ServicioComprobantePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getServicioComprobantePort(endpoint);
    }

    public ar.com.gestionit.ClienteWS.ServicioComprobante getServicioComprobantePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ar.com.gestionit.ClienteWS.ServicioComprobantePortBindingStub _stub = new ar.com.gestionit.ClienteWS.ServicioComprobantePortBindingStub(portAddress, this);
            _stub.setPortName(getServicioComprobantePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setServicioComprobantePortEndpointAddress(java.lang.String address) {
        ServicioComprobantePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ar.com.gestionit.ClienteWS.ServicioComprobante.class.isAssignableFrom(serviceEndpointInterface)) {
                ar.com.gestionit.ClienteWS.ServicioComprobantePortBindingStub _stub = new ar.com.gestionit.ClienteWS.ServicioComprobantePortBindingStub(new java.net.URL(ServicioComprobantePort_address), this);
                _stub.setPortName(getServicioComprobantePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ServicioComprobantePort".equals(inputPortName)) {
            return getServicioComprobantePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.gestionit.com.ar/", "ServicioComprobanteService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.gestionit.com.ar/", "ServicioComprobantePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ServicioComprobantePort".equals(portName)) {
            setServicioComprobantePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

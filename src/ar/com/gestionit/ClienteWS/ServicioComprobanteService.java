/**
 * ServicioComprobanteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ar.com.gestionit.ClienteWS;

public interface ServicioComprobanteService extends javax.xml.rpc.Service {
    public java.lang.String getServicioComprobantePortAddress();

    public ar.com.gestionit.ClienteWS.ServicioComprobante getServicioComprobantePort() throws javax.xml.rpc.ServiceException;

    public ar.com.gestionit.ClienteWS.ServicioComprobante getServicioComprobantePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

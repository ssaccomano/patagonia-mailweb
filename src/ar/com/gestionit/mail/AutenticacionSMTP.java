package ar.com.gestionit.mail;

import java.io.Serializable;

import javax.mail.PasswordAuthentication;

public class AutenticacionSMTP extends javax.mail.Authenticator implements Serializable{
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6855834916320880660L;
	String SMTP_AUTH_USER = "";
	String SMTP_AUTH_PWD  = "";
	
	public AutenticacionSMTP(String usr, String pass){
		SMTP_AUTH_USER = usr;
		SMTP_AUTH_PWD  = pass;
	}
	
	public PasswordAuthentication getPasswordAuthentication() {
		String username = SMTP_AUTH_USER;
		String password = SMTP_AUTH_PWD;
		return new PasswordAuthentication(username, password);
	}

}

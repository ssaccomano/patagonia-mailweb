package ar.com.gestionit.mail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.w3c.dom.Document;

import ar.com.gestionit.Exception.ExcepcionErrorConexionAS400;
import ar.com.gestionit.Exception.ExcepcionErrorConexionSMTP;
import ar.com.gestionit.Exception.ExcepcionErrorConexionWSSFirmas;
import ar.com.gestionit.Exception.ExcepcionErrorEnPDF;
import ar.com.gestionit.Exception.ExcepcionErrorEnParametros;
import ar.com.gestionit.Exception.ExcepcionErrorEnXML;
import ar.com.gestionit.Exception.ExcepcionFirmaInexistente;
import ar.com.gestionit.Exception.ExcepcionGeneral;
import ar.com.gestionit.Exception.ExcepcionTipoDeMailInvalido;
import ar.com.gestionit.dao.Emcabcp;
import ar.com.gestionit.dao.Emdetavi;
import ar.com.gestionit.dao.Emenvml;
import ar.com.gestionit.dao.Emparam;
import ar.com.gestionit.dao.Num;
import ar.com.gestionit.db.AccessDB;
import ar.com.gestionit.mail.alerta.ConexionAS400;
import ar.com.gestionit.util.Logs;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfEncryptor;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

/*Esta clase es un hilo que recibe un mailNumber y env�a el tipo mail, reemplazando 
 los campos variables, y adjuntando los documentos pdf, seg�n corresponda...*/
public class MailSender extends Thread implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 742074697759470717L;
	// private static final int BUFFER = 2048;
	private int mailNumber = 0;
	private int threadNumber = 0;
	private Document xmlDoc = null;
	private Emenvml datosEnvio = null;
	private Emparam parametros = null;
	private Num num = null;
	private Message msg = null;
	Multipart multipart = null;
	String fichero = "";
	private AccessDB db1 = new AccessDB();
	boolean flag = false;
	private static Log LOGGER = Logs.obtenerLog(MailSender.class);
	private long numeroDeOrdenDePago;
	private long subnumeroDeOrdenDePago;	
	ConexionAS400 conAS = new ConexionAS400();
	String flagEntorno = "";
	ResourceBundle em = null;
	
	// thread number est� para identificar los threads (no tiene utilidad,
	// sacarlo luego)
	public MailSender(int threadNumber) throws NumberFormatException,
			SQLException {
		this.threadNumber = threadNumber;
	}

	// Por la implementacion de Thread
	public void run() {
		em = ResourceBundle.getBundle("envioMail");
		String wait_for_mail = em.getString("wait_for_mail");
		flagEntorno = buscarFlagEntorno();
		
		int tipoError = 0;
						
		while (true) {
			try {				
				// Inicializo las variables
				// La conexi�n no la inicializo por cada run para que la reuse
				flag = false;
				fichero = "";
				multipart = new MimeMultipart("related");

				num = new Num();
								
				while (num.getNum().trim().equalsIgnoreCase("0")) {
					Thread.sleep(Integer.parseInt(wait_for_mail));
				}

				this.mailNumber = Integer.parseInt(num.getNum());
				datosEnvio = new Emenvml(num.getNum());

				// Comienza el procesamiento del mail a enviar
				postMail();

			}catch (ExcepcionErrorConexionAS400 e){
				tipoError = 10;
				conAS.setSinConexion(true);
				LOGGER.error("", e);	
			} catch (SQLException e) {				
				tipoError = 9;
				LOGGER.error("", e);
			} catch (InterruptedException e) {				
				tipoError = 9;
				LOGGER.error("", e);
			} catch (ExcepcionErrorEnParametros e) {
				tipoError = 2;
				LOGGER.error("", e);
			} catch (ExcepcionGeneral e) {				
				tipoError = 9;
				LOGGER.error("", e);
			} catch (ExcepcionErrorEnPDF e) {				
				tipoError = 4;
				LOGGER.error("", e);
			} catch (ExcepcionFirmaInexistente e) {				
				tipoError = 3;
				LOGGER.error("", e);
			} catch (ExcepcionErrorConexionWSSFirmas e) {				
				tipoError = 8;
				LOGGER.error("", e);
			} catch (ExcepcionErrorConexionSMTP e) {				
				tipoError = 5;				
				LOGGER.error("", e);
			} catch (ExcepcionErrorEnXML e) {
				tipoError = 7;
				LOGGER.error("", e);
			} catch (ExcepcionTipoDeMailInvalido e) {				
				tipoError = 6;
				LOGGER.error("", e);
			} catch (Exception e) {				
				tipoError = 9;
				LOGGER.error("", e);
			} catch (Throwable e) {				
				tipoError = 9;
				LOGGER.error("", e);
			}

			finally {

				Connection con = null;
				CallableStatement sqlCall = null;
				try {
					if(tipoError != 10){						
						con = db1.getConnection();
						sqlCall = con.prepareCall("CALL EM0002(?,?)");
						sqlCall.registerOutParameter(1, java.sql.Types.INTEGER);
						sqlCall.registerOutParameter(2, java.sql.Types.INTEGER);
	
						if (flag == true) {
							// Si envio OK -> mando al pgm que me da el num un 1
							// ejecuto el stored procedure
	
							sqlCall.setInt(1, Integer.parseInt(num.getNum()));
							sqlCall.setInt(2, 1);
							sqlCall.execute();
							sqlCall.close();
	
						} else {
							// Si envio ERROR -> mando al pgm que me da el num un 2
							// ejecuto el stored procedure						
							
							//Si el error fue de conexion con el AS400 no avisar tipo de error!!!
							
							sqlCall.setInt(1, Integer.parseInt(num.getNum()));
							sqlCall.setInt(2, tipoError);
							sqlCall.execute();
							sqlCall.close();
						}
					}
					
				}catch (ExcepcionErrorConexionAS400 e){
					tipoError = 10;
					conAS.setSinConexion(true);
					LOGGER.error("", e);

				} catch (Throwable e1) {					
					LOGGER.error("", e1);
					try {
						sqlCall.close();
					} catch (Throwable e) {						
						LOGGER.error("", e);
					}
				} finally {

					try {
						if(datosEnvio!=null) datosEnvio.cerrar();
						if(parametros!=null) parametros.cerrar();
					} catch (Throwable e) {						
						LOGGER.error("", e);
					}

					try {
						if (con != null && !con.isClosed()) {
							con.close();
						}
					} catch (Throwable e) {						
						LOGGER.error("", e);
					}
				}
			}
		}
	}

	// Env�a el mail...
	public void postMail() throws ExcepcionErrorEnParametros, ExcepcionGeneral,
			ExcepcionErrorEnPDF, ExcepcionErrorConexionWSSFirmas,
			ExcepcionFirmaInexistente, ExcepcionErrorConexionSMTP,
			ExcepcionErrorEnXML, ExcepcionTipoDeMailInvalido, RemoteException,
			ServiceException {
		boolean debug = false;

		ProcesadorHTML informacionMail = new ProcesadorHTML();
		informacionMail.init();
		System.out.println("----------- INICIO MAIL -------------------");
		System.out.println("Se procede a realizar un env�o del Emnum: " + this.mailNumber);
		// Set the host smtp address
		Properties props = new Properties();
		props.put("mail.smtp.host", informacionMail.getSMTP());
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.port", informacionMail.getPort());

		// create some properties and get the default Session
		Authenticator auth = new AutenticacionSMTP(informacionMail.getUsr(),
				informacionMail.getPass());

		Session session = Session.getDefaultInstance(props, auth);
		// Session session = Session.getDefaultInstance(props, null);
		session.setDebug(debug);		

		// Instancio el message
		msg = new MimeMessage(session);

		// Cargo los campos Para
		cargaCamposPara();
				
		// Cargo los campos CC
		cargaCamposCC();
		
		// Cargo los campos CCO
		cargaCamposCCO();
		
		// Cargo el origen del mail
		cargaDe();
		
		// Cargo el asunto del mail
		cargaAsunto();
		
		// Carga el cuerpo del mail
		cargaCuerpo(informacionMail);
		
		try {
			System.out.println("Es tipo : " + datosEnvio.getEmtipml());
			/*if(datosEnvio.getEmtipml() == 7 || datosEnvio.getEmtipml() == 1) {
				System.out.println("Es : " + datosEnvio.getEmtipml());
			}*/
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			if (datosEnvio.getEmexadj().trim().equalsIgnoreCase("S")) {
				// Carga los adjuntos al mail
				cargaAdjuntos();				
			}
		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}

		try {

			// Pongo las partes en el mensaje (adjuntos y cuerpo)
			msg.setContent(multipart);
			
			// Envio el mail
			Transport.send(msg);
			

		} catch (MessagingException e){			
			LOGGER.error("", e);			
			System.out.println("ERROR ENVIO: " + e.getMessage() );
			throw new ExcepcionErrorConexionSMTP(e.getMessage());
		}

		// Si se envia el mail correctamente pongo el flag en true
		flag = true;
		System.out.println("----------- FIN ENVIO --------------");
	}

	// Carga de destinatarios Para:
	private void cargaCamposPara() throws ExcepcionErrorEnParametros {

		try {
			InternetAddress[] addressTo = null;
			
			int aux = datosEnvio.getCantidadPara();
						
			if (aux !=0 && flagEntorno.trim().equalsIgnoreCase("S")){
				addressTo = destinatariosEntorno();
				
			}else {
				
				String recipients[] = new String[aux];
				System.out.println("Cantidad envios para : " + aux);
				switch (aux) {
				case 1:
					System.out.println("Para 1 : " + datosEnvio.getEmpara1());
					recipients[0] = datosEnvio.getEmpara1();					
					break;
	
				case 2:
					System.out.println("Para 1 : " + datosEnvio.getEmpara1());
					System.out.println("Para 2 : " + datosEnvio.getEmpara2());
					recipients[0] = datosEnvio.getEmpara1();
					recipients[1] = datosEnvio.getEmpara2();					
					break;
	
				case 3:
					System.out.println("Para 1 : " + datosEnvio.getEmpara1());
					System.out.println("Para 2 : " + datosEnvio.getEmpara2());
					System.out.println("Para 3 : " + datosEnvio.getEmpara3());
					recipients[0] = datosEnvio.getEmpara1();
					recipients[1] = datosEnvio.getEmpara2();
					recipients[2] = datosEnvio.getEmpara3();					
					break;
	
				default:
					throw new ExcepcionErrorEnParametros(
							"Error: El Campo \"Para\" esta vacio");
				}
				
				// Setteo los campos Para1, 2 y 3
				addressTo = new InternetAddress[recipients.length];
				for (int i = 0; i < recipients.length; i++) {
					addressTo[i] = new InternetAddress(recipients[i]);
				}
			}
			
			msg.setRecipients(Message.RecipientType.TO, addressTo);

		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (AddressException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}

	}

	// Carga de Destinatarios CC:
	private void cargaCamposCC() throws ExcepcionErrorEnParametros {

		try {
			
			InternetAddress[] addressCC = null;
			int aux = datosEnvio.getCantidadCCO();
			
			if (aux !=0 && flagEntorno.trim().equalsIgnoreCase("S")){
				addressCC = destinatariosEntorno();
				
			}else {
				
				String cc[] = new String[aux];
				System.out.println(" Cantidad de copias : " + aux);
	
				switch (aux) {
				case 1:
					cc[0] = datosEnvio.getEmcc1();
					System.out.println("Copia 1 : " + datosEnvio.getEmcc1());
					break;
	
				case 2:
					cc[0] = datosEnvio.getEmcc1();
					cc[1] = datosEnvio.getEmcc2();
					System.out.println("Copia 1 : " + datosEnvio.getEmcc1());
					System.out.println("Copia 2 : " + datosEnvio.getEmcc2());
					break;
	
				case 3:
					cc[0] = datosEnvio.getEmcc1();
					cc[1] = datosEnvio.getEmcc2();
					cc[2] = datosEnvio.getEmcc3();
					System.out.println("Copia 1 : " + datosEnvio.getEmcc1());
					System.out.println("Copia 2 : " + datosEnvio.getEmcc2());
					System.out.println("Copia 3 : " + datosEnvio.getEmcc2());
					break;
	
				default:
					break;
				}

				// Setteo los campos CC1, 2 y 3
				addressCC = new InternetAddress[cc.length];
				for (int i = 0; i < cc.length; i++) {
					addressCC[i] = new InternetAddress(cc[i]);
				}
			}
				
			msg.setRecipients(Message.RecipientType.CC, addressCC);

		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (AddressException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}
	}

	// Carga de Destinatarios CCO:
	private void cargaCamposCCO() throws ExcepcionErrorEnParametros {

		try {
			InternetAddress[] addressCCO = null;
			int aux = datosEnvio.getCantidadCCO();
			
			if (aux !=0 && flagEntorno.trim().equalsIgnoreCase("S")){
				addressCCO = destinatariosEntorno();
				
			}else {
				
				String cco[] = new String[aux];
				System.out.println(" Cantidad de copias Ocultas : " + aux);
				
				switch (aux) {
				case 1:
					cco[0] = datosEnvio.getEmcco1();
					System.out.println("Copia Oculta 1 : " + datosEnvio.getEmcco1());
					break;
	
				case 2:
					cco[0] = datosEnvio.getEmcco1();
					cco[1] = datosEnvio.getEmcco2();
					System.out.println("Copia Oculta 1 : " + datosEnvio.getEmcco1());
					System.out.println("Copia Oculta 2 : " + datosEnvio.getEmcco2());
					break;
	
				case 3:
					cco[0] = datosEnvio.getEmcco1();
					cco[1] = datosEnvio.getEmcco2();
					cco[2] = datosEnvio.getEmcco3();
					System.out.println("Copia Oculta 1 : " + datosEnvio.getEmcco1());
					System.out.println("Copia Oculta 2 : " + datosEnvio.getEmcco2());
					System.out.println("Copia Oculta 3 : " + datosEnvio.getEmcco3());
					break;
	
				default:
					break;
				}
	
				// Setteo los campos CCO1, 2 y 3
				addressCCO = new InternetAddress[cco.length];
				for (int i = 0; i < cco.length; i++) {
					addressCCO[i] = new InternetAddress(cco[i]);
				}
			}

			msg.setRecipients(Message.RecipientType.BCC, addressCCO);

		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (AddressException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}

	}

	// Cargo el origen del mail (Campo DE)
	private void cargaDe() throws ExcepcionErrorEnParametros {

		try {
			// set the from and to address
			InternetAddress addressFrom = new InternetAddress(datosEnvio
					.getEmde());

			msg.setFrom(addressFrom);

		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (AddressException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}

	}

	private void cargaAsunto() throws ExcepcionErrorEnParametros {

		try {
			// Setting the Subject and Content Type
			msg.setSubject(datosEnvio.getEmasunt().trim() + " - (" + mailNumber
					+ ")");

		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}

	}
	
	private void cargaCuerpo(ProcesadorHTML informacionMail)
			throws ExcepcionErrorEnParametros, ExcepcionErrorEnXML,
			ExcepcionTipoDeMailInvalido {
		try {
			if(datosEnvio == null)
				return;
			
			if(datosEnvio.getEmdetd() != null && datosEnvio.getEmdetd().trim().equalsIgnoreCase("S")) {
				cargaCuerpoDinamico(informacionMail);
			} else {
				cargaCuerpoEstatico(informacionMail);
			}
		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}
	}
	
	private void cargaCuerpoDinamico(ProcesadorHTML informacionMail)
			throws ExcepcionErrorEnParametros, ExcepcionErrorEnXML,
			ExcepcionTipoDeMailInvalido {
		try {
			//Obtengo los datos de EMPARAM
			Emdetavi emdetavi = new Emdetavi(num.getNum());
			
			xmlDoc = emdetavi.getEmdetaviXml();
			//Obtengo los datos de EMPARAM
			parametros = new Emparam(num.getNum());
			Document xmlDocParam = parametros.getEmparamXml();
			ArrayList<String> mail = informacionMail.getMailTipo(datosEnvio
					.getEmtipml(), xmlDoc, xmlDocParam);
			for (int i = 0; i < mail.size(); i++) {
				if (mail.get(i) != null)
					fichero += mail.get(i);
				else
					break;
			}
			
			fichero = informacionMail.getDynamicTable(xmlDoc, fichero,datosEnvio.getEmtipml());
			
			BodyPart texto = new MimeBodyPart();
			texto.setContent(fichero, "text/html");
			multipart.addBodyPart(texto);

			// Agrego imagen del logo del banco al cuerpo del mail
			MimeBodyPart logobco = new MimeBodyPart();						
			DataSource fds = new FileDataSource(Thread.currentThread()
					.getContextClassLoader().getResource(
							"/templates/logobcopat.JPG").getFile());
			logobco.setDataHandler(new DataHandler(fds));
			logobco.setHeader("Content-ID", "<image>");
			multipart.addBodyPart(logobco);

		} catch (IOException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (ExcepcionErrorConexionAS400 e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}
	}

	private void cargaCuerpoEstatico(ProcesadorHTML informacionMail)
			throws ExcepcionErrorEnParametros, ExcepcionErrorEnXML,
			ExcepcionTipoDeMailInvalido {

		try {
			//Obtengo los datos de EMPARAM
			parametros = new Emparam(num.getNum());
			
			xmlDoc = parametros.getEmparamXml();

			ArrayList<String> mail = informacionMail.getMailTipo(datosEnvio
					.getEmtipml(), xmlDoc, null);
			for (int i = 0; i < mail.size(); i++) {
				if (mail.get(i) != null)
					fichero += mail.get(i);
				else
					break;
			}

			BodyPart texto = new MimeBodyPart();
			texto.setContent(fichero, "text/html");
			multipart.addBodyPart(texto);

			// Agrego imagen del logo del banco al cuerpo del mail
			MimeBodyPart logobco = new MimeBodyPart();						
			DataSource fds = new FileDataSource(Thread.currentThread()
					.getContextClassLoader().getResource(
							"/templates/logobcopat.JPG").getFile());
			logobco.setDataHandler(new DataHandler(fds));
			logobco.setHeader("Content-ID", "<image>");
			multipart.addBodyPart(logobco);

		} catch (IOException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (ExcepcionErrorConexionAS400 e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}
	}

	private void cargaAdjuntos() throws ExcepcionErrorEnParametros,
			ExcepcionGeneral, ExcepcionErrorEnPDF, ExcepcionFirmaInexistente,
			ExcepcionErrorConexionWSSFirmas, RemoteException, ServiceException {
		
		ResourceBundle em = ResourceBundle.getBundle("envioMail");
				
		// Cargar Numero de Orden de Pago y SubOrden
		try {
			numeroDeOrdenDePago = Long.parseLong(datosEnvio.getNumOrdenPago());
		} catch (NumberFormatException e1) {
			LOGGER.error("", e1);
			throw new ExcepcionGeneral(e1.getMessage());
		} catch (SQLException e1) {
			LOGGER.error("", e1);
			throw new ExcepcionGeneral(e1.getMessage());
		}
		try {
			subnumeroDeOrdenDePago = Long.parseLong(datosEnvio
					.getNumSubOrdenPago());
		} catch (NumberFormatException e1) {
			LOGGER.error("", e1);
			throw new ExcepcionGeneral(e1.getMessage());
		} catch (SQLException e1) {
			LOGGER.error("", e1);
			throw new ExcepcionGeneral(e1.getMessage());
		}
				
		// Invocacion al WebService externo		
		ar.com.gestionit.ClienteWS.ServicioComprobanteServiceLocator locator = new ar.com.gestionit.ClienteWS.ServicioComprobanteServiceLocator();
		locator.getPort(ar.com.gestionit.ClienteWS.ServicioComprobante.class);
		locator.setServicioComprobantePortEndpointAddress(em.getString("WSS_WSCPADJ"));
		
		ar.com.gestionit.ClienteWS.Comprobante[] CmpAdj = locator.getServicioComprobantePort().getComprobante(
				numeroDeOrdenDePago, subnumeroDeOrdenDePago, mailNumber);

		// Adjunto del mail
		MimeBodyPart messageBodyPart = null;
		
		Emcabcp cabeceraComp = null;
		HashMap<String, String> nombreComprobantes = null;

		// Obtengo los datos de los comprobantes de la tabla de cabecera de
		// comprobantes EMCABCP
		try {
			cabeceraComp = new Emcabcp(num.getNum());
			nombreComprobantes = cabeceraComp.hs();
		}catch (ExcepcionErrorConexionAS400 e) {
			conAS.setSinConexion(true);
			LOGGER.error("", e);						
		} catch (SQLException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnPDF(e.getMessage());			
		} finally {
			cabeceraComp.cerrar();
		}

		// Obtengo los comprobantes de la tabla EMCPADJ en formato ByteArrayOutputStream
		try {
			PdfReader reader;
			ByteArrayOutputStream os = null;

			int numeroDeComprobate = 1;
			
			for (int i = 0; i < CmpAdj.length; i++) {
				
				//Si No hubo Error
				if(CmpAdj[i].getTipoError() == 0) {
				
					os = new ByteArrayOutputStream();
					reader = new PdfReader(CmpAdj[i].getPdf());
	
					PdfEncryptor.encrypt(reader, os, PdfWriter.STRENGTH40BITS, "",
							"gestionit", PdfWriter.AllowPrinting);
					
					javax.mail.util.ByteArrayDataSource ba = new ByteArrayDataSource(
							os.toByteArray(),
							"application/pdf; Content-Disposition= attachment; filename=\""
									+ nombreComprobantes.get(Integer
											.toString(numeroDeComprobate))
									+ ".pdf\";");
	
					messageBodyPart = new MimeBodyPart();				
					messageBodyPart.setDataHandler(new DataHandler(ba));
					// Setteo el nombre de archivo
					messageBodyPart.setFileName(nombreComprobantes.get(Integer.toString(numeroDeComprobate))+ ".pdf"); 
					// Agrego el attachment al mail a enviar
					multipart.addBodyPart(messageBodyPart);
					// Cierro el
					os.close();
					reader.close();
	
					numeroDeComprobate++;								
				} else {
					int tipoErrorWS = CmpAdj[i].getTipoError();
					
					switch (tipoErrorWS) {
					case 3: throw new ExcepcionFirmaInexistente("ExcepcionFirmaInexistente");
					case 4: throw new ExcepcionErrorEnPDF("ExcepcionErrorEnPDF");
					case 8: throw new ExcepcionErrorConexionWSSFirmas("ExcepcionErrorConexionWSSFirmas");					
					default: throw new Exception();
					}
				}
			}
				
		} catch (ExcepcionFirmaInexistente e) {
			LOGGER.error("", e);
			throw new ExcepcionFirmaInexistente(e.getMessage());
		} catch(ExcepcionErrorEnPDF e){			
			LOGGER.error("",e);			
			throw new ExcepcionErrorEnPDF(e.getMessage());
		} catch (ExcepcionErrorConexionWSSFirmas e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorConexionWSSFirmas(e.getMessage());			
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnPDF(e.getMessage());
		} catch (IOException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnPDF(e.getMessage());
		} catch (DocumentException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnPDF(e.getMessage());
		} catch (Exception e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnPDF(e.getMessage());
		}

	}
	
	private String buscarFlagEntorno () {
		
		try {
			if (em.getString("entornoPrueba") != null) {
				flagEntorno = em.getString("entornoPrueba");
			}
		}catch (Exception e) {
			//error al leer el Flag de Entorno
			flagEntorno = "";
		}
		return flagEntorno;
		
	}
	
	private InternetAddress[] destinatariosEntorno () throws AddressException, ExcepcionErrorEnParametros {
				
		int cantDest = getCantidadDestinatarios();
		
		String recipients[] = new String[cantDest];
		
		switch (cantDest) {
		case 1:
			recipients[0] = em.getString("destinatario1").trim();
			break;

		case 2:
			recipients[0] = em.getString("destinatario1").trim();
			recipients[1] = em.getString("destinatario2").trim();
			break;
			
		default:
			throw new ExcepcionErrorEnParametros("Error: El Campo \"Para\" esta vacio");
		}
		
		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}
		
		return addressTo;
	}
	
	private int getCantidadDestinatarios(){
		
		int cantidad = 0;
		try {
			if(em.getString("destinatario1").trim().equalsIgnoreCase("")){
				cantidad = 0;
			}else {
				cantidad = 1;
			}
			
		}catch (Exception e) {
			LOGGER.error("Error: al leer los destinatarios de Entorno, en archivo de propiedades", e);
		}
		
		if (cantidad != 0) {
			try {
				if (!em.getString("destinatario2").trim().equalsIgnoreCase("")){
					cantidad = 2;
				}
				
			}catch (Exception e) {
				//LOGGER.error("Error: al leer el destinatario2 de Entorno, en archivo de propiedades, el campo no existe", e);
			}
		}
		
		return cantidad;
		
	}

}

package ar.com.gestionit.mail.alerta;

import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import ar.com.gestionit.mail.MailSender;
import ar.com.gestionit.quartz.SchedulerMAilAlert;
import ar.com.gestionit.quartz.TimerJob;
import ar.com.gestionit.util.Logs;

public class ConexionAS400 {
	
	public static boolean sinConexion = false;
	private static Log LOGGER = Logs.obtenerLog(MailSender.class);
	
	public ConexionAS400() {
		
		// TODO Auto-generated constructor stub
	}

	public boolean isSinConexion() {
		return sinConexion;
	}

	public void setSinConexion(boolean sinConexion){				
		this.sinConexion = sinConexion;
		
		if (isSinConexion()){
			if (SchedulerMAilAlert.sched != null){
				if (!SchedulerMAilAlert.inicializado) {					
					SchedulerMAilAlert.inicializarSched();					
					SchedulerMAilAlert.inicializado = true;
				}
			}
		}
	}

}

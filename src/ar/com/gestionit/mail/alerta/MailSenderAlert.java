package ar.com.gestionit.mail.alerta;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ar.com.gestionit.Exception.ExcepcionErrorConexionSMTP;
import ar.com.gestionit.Exception.ExcepcionErrorConexionWSSFirmas;
import ar.com.gestionit.Exception.ExcepcionErrorEnPDF;
import ar.com.gestionit.Exception.ExcepcionErrorEnParametros;
import ar.com.gestionit.Exception.ExcepcionErrorEnXML;
import ar.com.gestionit.Exception.ExcepcionFirmaInexistente;
import ar.com.gestionit.Exception.ExcepcionGeneral;
import ar.com.gestionit.Exception.ExcepcionTipoDeMailInvalido;

import ar.com.gestionit.mail.AutenticacionSMTP;
import ar.com.gestionit.mail.ProcesadorHTML;
import ar.com.gestionit.quartz.TimerJob;
import ar.com.gestionit.util.Logs;


/*Esta clase es un hilo que recibe un mailNumber y env�a el tipo mail, reemplazando 
 los campos variables, y adjuntando los documentos pdf, seg�n corresponda...*/
public class MailSenderAlert implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 742074697759470717L;
	
	private Document xmlDoc = null;		
	private Message msg = null;
	Multipart multipart = null;
	String fichero = "";
	boolean flag = false;
	private static Log LOGGER = Logs.obtenerLog(MailSenderAlert.class);	
	ResourceBundle rb = ResourceBundle.getBundle("mailAlertInfo");	
	private int horaAlert;
	private int minAlert;

	public MailSenderAlert() throws NumberFormatException, SQLException {
	}

	// Por la implementacion de Thread
	public void run() {
		// ResourceBundle em = ResourceBundle.getBundle("envioMailAlerta");
		// String wait_for_mail = em.getString("wait_for_mail");

		int tipoError = 0;
		int cond = 1;
		
		//Seteo Hora y Minutos de la ultima ejecucion del envio de alerta
//		setHoraAlert(date.getHours());
//		setMinAlert(date.getMinutes());
		
		ConexionAS400 conAS = new ConexionAS400();

		while (cond == 1) {
			try {
				cond = 0;
												
			    //Si tiene Conexion, setea en falso para que no env�e mails de alerta
				conAS.setSinConexion(false);
				
				// Inicializo las variables
				// La conexi�n no la inicializo por cada run para que la reuse
				flag = false;
				fichero = "";
				multipart = new MimeMultipart("related");
				
				// Comienza el procesamiento del mail a enviar
				postMail();

			} catch (ExcepcionErrorEnParametros e) {
				tipoError = 2;
				LOGGER.error("", e);
			} catch (ExcepcionGeneral e) {
				tipoError = 9;
				LOGGER.error("", e);
			} catch (ExcepcionErrorEnPDF e) {
				tipoError = 4;
				LOGGER.error("", e);
			} catch (ExcepcionFirmaInexistente e) {
				tipoError = 3;
				LOGGER.error("", e);
			} catch (ExcepcionErrorConexionWSSFirmas e) {
				tipoError = 8;
				LOGGER.error("", e);
			} catch (ExcepcionErrorConexionSMTP e) {
				tipoError = 5;
				LOGGER.error("", e);
			} catch (ExcepcionErrorEnXML e) {
				tipoError = 7;
				LOGGER.error("", e);
			} catch (ExcepcionTipoDeMailInvalido e) {
				tipoError = 6;
				LOGGER.error("", e);
			} catch (Exception e) {
				tipoError = 9;
				LOGGER.error("", e);
			} catch (Throwable e) {
				tipoError = 9;
				LOGGER.error("", e);
			}			
		}
	}

	// Env�a el mail...
	public void postMail() throws ExcepcionErrorEnParametros, ExcepcionGeneral,
			ExcepcionErrorEnPDF, ExcepcionErrorConexionWSSFirmas,
			ExcepcionFirmaInexistente, ExcepcionErrorConexionSMTP,
			ExcepcionErrorEnXML, ExcepcionTipoDeMailInvalido, RemoteException,
			ServiceException {
		boolean debug = false;

		ProcesadorHTML informacionMailAlert = new ProcesadorHTML();
		informacionMailAlert.init();
		/*
		 * CREAR TODO PARA LA NUEVA CONEXION DEL MAIL
		 */
		// Set the host smtp address
		Properties props = new Properties();
		props.put("mail.smtp.host", informacionMailAlert.getSMTP());
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.port", informacionMailAlert.getPort());

		// create some properties and get the default Session
		Authenticator auth = new AutenticacionSMTP(informacionMailAlert
				.getUsr(), informacionMailAlert.getPass());

		Session session = Session.getDefaultInstance(props, auth);
		// Session session = Session.getDefaultInstance(props, null);
		session.setDebug(debug);

		// Instancio el message
		msg = new MimeMessage(session);

		// Cargo los campos Para
		cargaCamposPara();
		
		// Cargo el origen del mail
		cargaDe();

		// Cargo el asunto del mail
		cargaAsunto();

		// Carga el cuerpo del mail
		cargaCuerpo(informacionMailAlert);
		
		try {
			// Pongo las partes en el mensaje (adjuntos y cuerpo)
			msg.setContent(multipart);

			// Envio el mail
			Transport.send(msg);

		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorConexionSMTP(e.getMessage());
		}

		// Si se envia el mail correctamente pongo el flag en true
		flag = true;

	}

	// Carga de destinatarios Para:
	private void cargaCamposPara() throws ExcepcionErrorEnParametros {

		try {
			String recipients[] = new String[1];
			try {
				recipients[0] = rb.getString("para1");
			} catch (Exception e) {
				throw new ExcepcionErrorEnParametros(
						"Error: El Campo \"Para1\" esta vacio");
			}

			// Setteo el campos Para1
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				addressTo[i] = new InternetAddress(recipients[i]);
			}

			msg.setRecipients(Message.RecipientType.TO, addressTo);

		} catch (AddressException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}
	}

	// Cargo el origen del mail (Campo DE)
	private void cargaDe() throws ExcepcionErrorEnParametros {

		try {
			// set the from and to address
			InternetAddress addressFrom = new InternetAddress(rb
					.getString("remitente"));

			msg.setFrom(addressFrom);

		} catch (AddressException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}
	}

	private void cargaAsunto() throws ExcepcionErrorEnParametros {

		try {
			// Setting the Subject and Content Type
			msg.setSubject(rb.getString("asunto"));

		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}

	}

	private void cargaCuerpo(ProcesadorHTML informacionMailAlert)
			throws ExcepcionErrorEnParametros, ExcepcionErrorEnXML,
			ExcepcionTipoDeMailInvalido {
		
		try {

			try {
				xmlDoc = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder().parse(
								new InputSource(new StringReader(
										"<root></root>")));
			} catch (SAXException e) {
				LOGGER.error("", e);				
			} catch (ParserConfigurationException e) {
				LOGGER.error("", e);
			}
			
			int tipoMailAlert = Integer.parseInt(rb.getString("tipoMailAlert"));

			ArrayList<String> mail = informacionMailAlert.getMailTipo(
					tipoMailAlert, xmlDoc, null);
			for (int i = 0; i < mail.size(); i++) {
				if (mail.get(i) != null)
					fichero += mail.get(i);
				else
					break;
			}

			BodyPart texto = new MimeBodyPart();
			texto.setContent(fichero, "text/html");
			multipart.addBodyPart(texto);

			// Agrego imagen del logo del banco al cuerpo del mail
			MimeBodyPart logobco = new MimeBodyPart();
			DataSource fds = new FileDataSource(Thread.currentThread()
					.getContextClassLoader().getResource(
							"/templates/logobcopat.JPG").getFile());
			logobco.setDataHandler(new DataHandler(fds));
			logobco.setHeader("Content-ID", "<image>");
			multipart.addBodyPart(logobco);

		} catch (IOException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		} catch (MessagingException e) {
			LOGGER.error("", e);
			throw new ExcepcionErrorEnParametros(e.getMessage());
		}
	}
			
	public int getHoraAlert() {
		return horaAlert;
	}

	public void setHoraAlert(int horaAlert) {
		this.horaAlert = horaAlert;
	}

	public int getMinAlert() {
		return minAlert;
	}

	public void setMinAlert(int minAlert) {
		this.minAlert = minAlert;
	}
}

package ar.com.gestionit.mail;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.regex.*;

import org.apache.commons.logging.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ar.com.gestionit.Exception.ExcepcionTipoDeMailInvalido;
import ar.com.gestionit.encrypt.EncryptFactory;
import ar.com.gestionit.templates.TemplatesHTML;
import ar.com.gestionit.util.Logs;

/*La funcionalidad de esta clase es levantar en memoria (mediante el m�todo init) toda 
 * la informaci�n necesaria para el env�o de los mails(inclusive los templates HTML).*/

public class ProcesadorHTML implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9073068971467190288L;
	
	private static final String DYNAMIC_CONTENT_TAG = "[[DYNAMIC-TABLE]]";
	private static final String DYNAMIC_CONTENT_STYLE = "dynamic-content";

	public String smtp = "";

	public String port = "";

	public String usr = "";

	public String pass = "";

	private boolean cumpleCondicion = true;
	private ArrayList<String> retorno;
	private Pattern patron;
	private Matcher encaja;
	private int i;
	private boolean esPrimeraLinea;
	private boolean flag;
	private String resultado;
	private int tempRenglon;
	private static Log LOGGER = Logs.obtenerLog(MailSender.class);

	/*
	 * El template HTML de cada tipo de mail va a ir en un ArrayList de strings,
	 * cada l�nea del HTML es un elemento del ArrayList
	 */
	// public ArrayList<ArrayList<String>> mailTipo = new
	// ArrayList<ArrayList<String>>();
	// TemplatesHTML mailTipo;
	// El m�todo init levanta todo en memoria
	public void init() {

		ResourceBundle rb = ResourceBundle.getBundle("mailInfo");
		smtp = rb.getString("smtp");
		port = rb.getString("port");
		try {
			EncryptFactory ef = new EncryptFactory("DES");
			pass = ef.decrypt(rb.getString("pass"));
			usr	= ef.decrypt(rb.getString("usr"));

		} catch (IllegalArgumentException e) {			
		}
		catch (Exception e) {
			LOGGER.error("",e);
		}
		// setMailTipo();
	}

	// Devuelve el STMP
	public String getSMTP() {
		return smtp;
	}

	// Devuelve el puerto
	public int getPort() {
		return Integer.parseInt(port);
	}

	// Devuelve el nombre de usuario
	public String getUsr() {
		return usr;
	}

	// Devuelve el password
	public String getPass() {
		return pass;
	}

	public String getDynamicTable(Document campos, String email, int codTemplate) {
		StringBuffer result = new StringBuffer();
		NodeList nodes = campos.getChildNodes().item(0).getChildNodes();

		//Genero tabla
		result.append("<table class=\"" + DYNAMIC_CONTENT_STYLE + "\">");

		//Genero cabecera de tabla
		result.append("<tr>");
		int size = nodes.getLength();
		int columnAmount;
		NodeList rowContent;
		rowContent = nodes.item(0).getChildNodes();
		columnAmount = rowContent.getLength();
		for(int i = 0; i < columnAmount; i++) {
			result.append("<td>").append(rowContent.item(i).getNodeName()).append("</td>");
		}
		result.append("</tr>");
		String msgAdicionalPre = "";
		String msgAdicionalPost = "";
		//Genero el contenido
		for(int j = 0; j < size; j++) {
			result.append("<tr>");
			rowContent = nodes.item(j).getChildNodes();
			columnAmount = rowContent.getLength();
			for(int i = 0; i < columnAmount; i++) {
				//Seteo el texto para cada fila del template 28
				if(codTemplate == 28) {
					msgAdicionalPre = "El convenio de la l�nea " ;
					msgAdicionalPost = " del archivo recibido es incorrecto"; 
				}
				result.append("<td>").append(msgAdicionalPre).append(rowContent.item(i).getTextContent()).append(msgAdicionalPost).append("</td>");
			}
			result.append("</tr>");
		}

		result.append("</table>");
		
		if(email != null)
			return email.replace(DYNAMIC_CONTENT_TAG, result.toString());
		return result.toString();
	}

	// Recibe el n�mero del tipo de mail y los campos a utilizar. Devuelve el
	// mail en HTML listo para su env�o
	public ArrayList<String> getMailTipo(int tipo, Document campos, Document camposEmparam)
			throws IOException, ExcepcionTipoDeMailInvalido {

		retorno = new ArrayList<String>();
		patron = Pattern.compile("\\*\\*\\*[^\\*]+\\*\\*\\*");

		String tempLinea = "";

		// Recorro cada linea del mail
		for (i = 0; i < TemplatesHTML.get(tipo).size(); i++) {
			tempLinea = TemplatesHTML.get(tipo).get(i);
			if (tempLinea != null) {
				// busco campos variables e if

				encaja = patron.matcher(tempLinea);
				
				while (encaja.find()) {
					resultado = encaja.group();
					resultado = resultado.substring(3, resultado.length() - 3);

					if (!resultado.substring(0, 1).equals("@")) {
						// Si no hay @ es un campo variable..

						//Si es una tabla dinamica utilizo el emparam para buscar las variables en vez de Emdetavi 
						if(tipo == 28) {
							// Reemplazo la variable encontrada por el tag que
							// obtuve del AS400 del emparam
							tempLinea = reemplazarPorValorTag(camposEmparam, tempLinea);
						}
						else {
							// Reemplazo la variable encontrada por el tag que
							// obtuve del AS400
							tempLinea = reemplazarPorValorTag(campos, tempLinea);
						}
					} else {
						// Si hay @ es un SI, SINO o FIN

						// if (resultado != null) {
						// Node node =
						// campos.getElementsByTagName(resultado).item(0);
						//
						// // Si el resultado no existe en el xml,
						// // entonces auxCampo toma el valor del resultado
						// auxCampo = node != null ?
						// node.getTextContent().toString().trim() : resultado;

						if (resultado.substring(0, 3).equalsIgnoreCase("@SI")) {
							// Si es @SI
							tempLinea = procesarSi(tempLinea, campos, tipo);

						} else {
							// No es @SI

							if (resultado.equalsIgnoreCase("@FIN")) {
								// Procesar @FIN
								if (!cumpleCondicion) {
									cumpleCondicion = true;
								}
								tempLinea = tempLinea.replaceFirst("\\*\\*\\*"
										+ resultado + "\\*\\*\\*", "");
							} else if (resultado.equalsIgnoreCase("@SN")) {
								// Venia en un SI que cumplia y encontro un
								// SINO, debe ignorarlo
								// Procesa @SN
								// tempLinea = procesarSN(tempLinea, campos);
								tempRenglon = tempLinea.indexOf("***@SN");
								tempLinea = procesarHastaFinOSinoSinAgregar(
										tempLinea, tipo);
							}
						}

					}
				}
				if (cumpleCondicion) {
					retorno.add(tempLinea);
				}
			}
		}
		return retorno;
	}

	private String procesarSi(String tempLinea, Document campos, int tipo) throws ExcepcionTipoDeMailInvalido {

		// Index Of del @SI
		// int tempRenglon = 0;

		boolean flagNoEsBlanco = false;
		// boolean esPrimeraLinea = true;

		// Guardo el indico del @SI
		if (tempLinea.contains("***@SI")) {
			tempRenglon = tempLinea.indexOf("***@SI");
			esPrimeraLinea = true;
		}

		// Proceso los & -> por ahora en desuso
		if (resultado.substring(resultado.length() - 1, resultado.length())
				.toString().trim().equalsIgnoreCase("&")) {

			if (!campos.getElementsByTagName(
					resultado.substring(4, resultado.length() - 2)).item(0)
					.getTextContent().toString().trim().equalsIgnoreCase("")) {
				flagNoEsBlanco = true;
			}

		}
		// Fin de proceso los & -> por ahora en desuso

		// Veo si el si se cumple o no		
		if (flagNoEsBlanco
				|| resultado.substring(resultado.indexOf("=") + 1,
						resultado.length()).toString().trim().equalsIgnoreCase(
								campos.getElementsByTagName(resultado.substring(4, resultado.indexOf("="))).item(0).getTextContent().toString().trim())) {

			// Cumple el @SI -> Agrego las lineas hasta el @FIN
			cumpleCondicion = true;

			// Si encuentro un sino lo ignoro
			tempLinea = tempLinea.replaceFirst("\\*\\*\\*" + resultado + "\\*\\*\\*", "");

		} else {
			// No cumple el @SI requerido
			/**
			 * caso 1: guardar del inicio de la lina hasta el
			 * 
			 * @si que no cumple
			 * 
			 * caso 2: guardar desde el
			 * @fin en adelante
			 * 
			 * caso 3: guardar despues del
			 * @fin que no cumple, desde el
			 * @si que cumple hasta el fin de la linea
			 * 
			 */

			tempLinea = procesarHastaFinOSinoSinAgregar(tempLinea, tipo);

			cumpleCondicion = true;
		}
		return tempLinea;
	}

	private String procesarFinSinAgregar(String tempLinea) {

		// Si entro aca, es que encontro el
		// @FIN del @SI que no cumplia

		if (tempLinea.length() == tempLinea.indexOf(resultado) + 7) {
			// Termina en ese @FIN
			// tempLinea = "";
			// retorno.add(tempLinea);

			if (esPrimeraLinea) {
				retorno.add(tempLinea.substring(0, tempRenglon));
				tempRenglon = 0;
				tempLinea = "";
			} else {
				tempLinea = "";
				retorno.add(tempLinea);
			}

		} else {
			// Vienen cosas despues del @FIN
			// Copio desde despues del @FIN
			// hasta el final

			if (esPrimeraLinea) {
				tempLinea = tempLinea.substring(0, tempRenglon)
						+ tempLinea.substring(tempLinea.indexOf(resultado) + 7);
			} else {
				tempLinea = tempLinea
						.substring(tempLinea.indexOf(resultado) + 7);
			}
		}
		flag = true;

		return tempLinea;
	}

	private String reemplazarPorValorTag(Document campos, String tempLinea) {

		String auxCampo = null;

		// busco el valor del campo
		if (resultado != null) {
			Node node = campos.getElementsByTagName(resultado).item(0);

			// Si el resultado no existe en el xml,
			// entonces auxCampo toma el valor del resultado
			auxCampo = node != null ? node.getTextContent().toString().trim()
					: resultado;
		}
		// (auxCampo!= null)
		if (auxCampo.length() > 0) {

			// escapar el simbolo $
			if (auxCampo.contains("$"))
				auxCampo = auxCampo.replace("$", "\\$");

			// reemplazo por el campo variable que corresponde
			tempLinea = tempLinea.replaceFirst("\\*\\*\\*" + resultado
					+ "\\*\\*\\*", auxCampo);
		} else {
			tempLinea = tempLinea.replaceFirst("\\*\\*\\*" + resultado
					+ "\\*\\*\\*", "");
		}

		return tempLinea;

	}

	private String procesarHastaFinOSinoSinAgregar(String tempLinea, int tipo) throws ExcepcionTipoDeMailInvalido {

		// Este flag indica (si esta en true) que se encontro un SI que no
		// cumple y no hay que agregar ninguna
		// Cosa que venga despues a menos que sea un SINO
		// Si esta en false indica que se termino el SI que no cumple

		flag = false;

		do {
			while (!flag && encaja.find()) {
				resultado = encaja.group();
				resultado = resultado.substring(3, resultado.length() - 3);
				if (encaja.group().substring(3, encaja.group().length() - 3)
						.equalsIgnoreCase("@FIN")) {

					tempLinea = procesarFinSinAgregar(tempLinea);

				} else if (encaja.group().substring(3,
						encaja.group().length() - 3).equalsIgnoreCase("@SN")) {
					// Si es un SINO

					// Es la primer linea y no encuentro m�s ***
					// -> agrego lo que ven�a antes del @SI que
					// no cumple
					if (esPrimeraLinea && !flag) {
						retorno.add(tempLinea.substring(0, tempRenglon));
						tempRenglon = 0;
					}

					int tempRenglonSN = tempLinea.indexOf("***@SN");
					String aux = tempLinea.substring(tempRenglon,
							tempRenglonSN + 9);
					tempLinea = tempLinea.replace(aux, "");

					// Cumple el @SI -> Agrego las lineas hasta el @FIN
					cumpleCondicion = true;

					return tempLinea;
				}

			}
			// Es la primer linea y no encuentro m�s ***
			// -> agrego lo que ven�a antes del @SI que
			// no cumple
			if (esPrimeraLinea && !flag) {
				retorno.add(tempLinea.substring(0, tempRenglon));
				tempRenglon = 0;
			}

			// Si encontre o no encontre el @FIN, de
			// cualquier manera me muevo a la siguiente
			// linea
			// Y muevo el matcher para que en el bucle
			// grande de afuera vuelva a procesar
			if (flag) {
				// Si encontr� el @FIN y la linea tiene
				// mas cosas depues de el
				// no muevo la linea para que la procese
				// el bucle de afuera
				if (tempLinea.equalsIgnoreCase("")) {
					i++;
					tempLinea = TemplatesHTML.get(tipo).get(i);
					esPrimeraLinea = false;
					if (tempLinea != null) {
						Pattern patron2 = Pattern
								.compile("\\*\\*\\*[^\\*]+\\*\\*\\*");
						encaja = patron2.matcher(tempLinea);
					}
				}
			} else {
				i++;
				tempLinea = TemplatesHTML.get(tipo).get(i);
				esPrimeraLinea = false;
				if (tempLinea != null) {
					Pattern patron2 = Pattern
							.compile("\\*\\*\\*[^\\*]+\\*\\*\\*");
					encaja = patron2.matcher(tempLinea);
				}
			}

		} while (!flag);

		return tempLinea;

	}

}

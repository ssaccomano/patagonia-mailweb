package ar.com.gestionit.templates;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;

import ar.com.gestionit.Exception.ExcepcionTipoDeMailInvalido;
import ar.com.gestionit.util.Logs;

public abstract class TemplatesHTML implements Serializable{
	
	private static ArrayList<ArrayList<String>> mailTipo = new ArrayList<ArrayList<String>>();
	private static Log LOGGER = Logs.obtenerLog(TemplatesHTML.class);
	
	
	public static void levantarTemplates() {
		try {
			
			ResourceBundle rb = ResourceBundle.getBundle("templatesMapping");
						
			//int cont = 1;
			
			for (int x = 0; x < 100; x++) {	
				try{
					
					rb.getString(Integer.toString(x));
					
					String path = rb.getString(Integer.toString(x)).trim();
					
					//Levanto el Template correspondiente
					BufferedReader br = new BufferedReader(new InputStreamReader(TemplatesHTML.class.getResourceAsStream(path)));
					String linea = "";
					int i = 0;
					ArrayList<String> aux = new ArrayList<String>();
					while ((linea = br.readLine()) != null) {
						if (!linea.trim().equals("")) {
							aux.add(linea);
							i++;
						}
					}
					br.close();
					mailTipo.add(aux);
					
				}catch (MissingResourceException e){
					mailTipo.add(null);
				}
				
			}
				
		} catch (Exception e) {
			LOGGER.error("", e);
		}
	}
	
	public static ArrayList<String> get(int i) throws ExcepcionTipoDeMailInvalido{
		
		if(mailTipo.get(i)==null){
			throw new ExcepcionTipoDeMailInvalido("Error: Tipo de mail invalido!");
		}
		
		return mailTipo.get(i);	
	}
	
	
}
